import React, {useEffect} from "react";
import {getAuthState} from "./store/actions/userActions";
import { useDispatch, useSelector } from "react-redux";
import AuthProvider from "./HOCs/AuthProvider";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAuthState())
  }, []);

  return (
    <AuthProvider />
  );
};

export default App;
