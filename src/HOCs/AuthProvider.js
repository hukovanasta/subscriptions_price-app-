import {useSelector} from "react-redux";
import PublicRouter from "../routes/PublicRouter";
import PrivateRouter from "../routes/PrivateRouter";
import {Spin} from "antd";
import '../pages/home/Home.css';

const AuthProvider = () => {
  const { authenticated, isCheckAuthStateInProgress } = useSelector((state) => state.user)
    return (
      <div>
        { isCheckAuthStateInProgress ?
          (<Spin size="large" className="spin-style" />) :
          authenticated ?
            (<PrivateRouter />) : (<PublicRouter />)
        }
     </div>
    )
}

export default AuthProvider