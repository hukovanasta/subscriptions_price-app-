import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import './Header.css';
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";
import 'antd/dist/antd.css';
import { PageHeader, Space} from 'antd';
import { useSelector } from "react-redux";

const Header = ( ) => {
  const { authenticated  } = useSelector((state) => state.user);

  return (
    <div className="container">
      {/* <PageHeader
        className="header"
        extra={[
          <Space  align="center" size={780}>
            {authenticated ? (
              <Link to="/" className="header-logo" >Substrictions</Link>
                ) : (
                <Space size={180}>
                  <Link to="/login" className="header-logo">Log In</Link>
                  <Space size={50}> <Link to="/register" className="header-logo">Sign Up</Link></Space>
               </Space>
            )}
          </Space>
        ]}
      /> */}
      {authenticated ? (
        <Fragment>
          <Link to="/" className="header-logo" >Substrictions</Link>
        </Fragment>
          ) : (
        <Fragment>
            <Space size={180}>
              <Link to="/login" className="header-logo">Log In</Link>
              <Space size={50}> <Link to="/register" className="header-logo">Sign Up</Link></Space>
            </Space>
        </Fragment>
       )}
    </div>
  );
}

// export default connect((state) => {
//   return { authenticated: state.user.authenticated }
// })(Header);
export default Header;