import React from 'react';
import { Card, Col } from 'antd';

function SubstrictionCard(props){
  const date = new Date(props.subscription.dueDate).toLocaleDateString();
  return(
    <Col>
      <Card
        title={props.subscription.name}
        extra={<a href="#"><img alt="example" src={props.subscription.icon}/></a>}
        style={{ width: 300 , margin: '30px 0' }}>
          <p> price for month: {props.subscription.price}
            <span>{props.subscription.currency.icon}</span>
          </p>
          <p> due date: <span>{date}</span></p>
      </Card>
    </Col>

  )
}

export default SubstrictionCard;