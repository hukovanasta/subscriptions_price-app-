import React, { useEffect} from 'react'
import { useDispatch, useSelector } from "react-redux";
import { Row } from 'antd';
import {loadSubscriptions} from '../../store/actions/subscriptionsActions';
import SubstrictionCard from '../substriction_card/SubstrictionCard';

function SubstrictionList(){

  const { subscriptions } = useSelector((state) => state.subscriptions);

  const dispatch = useDispatch();

  useEffect(()=> {
    dispatch(loadSubscriptions())
  }, [dispatch]);

  return(
    <div>
      <Row justify="center" gutter={12}>
        {subscriptions.map((subscription) =>
          (<SubstrictionCard
             subscription = {subscription}
             key = {subscription.id}
           />
          ))
        }
      </Row>
    </div>
  )
}

export default SubstrictionList;