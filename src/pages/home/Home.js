import React from 'react'
import './Home.css';
import 'antd/dist/antd.css';
import { Typography, Button } from 'antd';
import {logOut} from '../../store/actions/userActions';
import { useDispatch } from "react-redux";
import SubstrictionList from '../../components/substriction_list/SubstrictionList';

const { Title } = Typography;

const Home = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logOut())
  };

  return (
    <div className="homeHeader">
      <div className="homeContainer">
       <Title className="homeTitle" >
         Your subscriptions
       </Title>
       <div className="substrictions">
         <SubstrictionList/>
       </div>
       <Button  className="home-btn" type="dashed"  onClick={handleLogout}>Sign out</Button>
      </div>
    </div>
  );
}

export default Home;