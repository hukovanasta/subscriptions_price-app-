import React, {useCallback} from "react";

import 'antd/dist/antd.css';
import './LogIn.css';
import {logIn} from '../../store/actions/userActions';
import {Button, Card, Col, Form, Input, Row, Space} from "antd";
import {
  Link
} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

const LogIn = () => {
  const { isLoginInProgress } = useSelector((state) => state.user)
  const dispatch = useDispatch()

  const onFinish = useCallback((evt) => {
    dispatch(logIn({ email: evt.username, password: evt.password }))
  }, [])

  const onFinishFailed = useCallback((evt) => {
    console.log(evt)
  }, [])

  return (
    <Row justify="center">
      <Col span={48}>
        <Card title="Log In" style={{ width: '100%', margin: '100px 0' }}>
          <Form
              style={{ width: '400px' }}
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
          >
            <Form.Item
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit" loading={isLoginInProgress}>
                Submit
              </Button>
            </Form.Item>
            <Link to="/auth/signup" className="header-link" style={{ margin: '0 0 0 140px' }}>If you  haven't account?</Link>
          </Form>
        </Card>

      </Col>
    </Row>
  );
};

export default LogIn