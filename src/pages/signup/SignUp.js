import React, {useCallback} from "react";
import 'antd/dist/antd.css';
import './SignUp.css';
import {signUp} from '../../store/actions/userActions'
import {Button, Card, Col, Form, Input, Row} from "antd";
import {
  Link
} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";


const SignUp = () => {
  const { isSignupInProgress } = useSelector((state) => state.user)
  const dispatch = useDispatch()
  const onFinish = useCallback((evt) => {
    dispatch(signUp({ email: evt.username, password: evt.password }))
  }, [])

  const onFinishFailed = useCallback((evt) => {
    console.log(evt)
  }, [])
  const [form] = Form.useForm();

  return (
    <Row justify="center">
    <Col span={48}>
      <Card title="Sign up" style={{ width: '100%', margin: '100px 0' }}>
        <Form
          style={{ width: '420px' }}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          form={form}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: 'Please input your password!' }]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            className="card-item-title"
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                  },
                }),
              ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit" loading={ isSignupInProgress }>
            Submit
          </Button>
          </Form.Item>
          <Link to="/auth/login" className="header-link" style={{ margin: '0 0 0 140px' }} >If you  have account?</Link>
        </Form>
      </Card>
    </Col>
  </Row>
  );
};

export default SignUp