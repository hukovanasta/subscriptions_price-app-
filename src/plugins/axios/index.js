import axios from 'axios';
import interceptors from './interceptors';

const api = axios.create({ baseURL: `http://localhost:3333` });
interceptors(api);

export default api;

