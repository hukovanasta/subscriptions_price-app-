export default (function (axios) {
  axios.interceptors.request.use(
    async config => {
      const keys = localStorage.getItem('token')
      config.headers = {
        'authorization': `Bearer ${keys}`,
      }
      return config;
    },
    error => {
      return Promise.reject(error)
    }
  )
});