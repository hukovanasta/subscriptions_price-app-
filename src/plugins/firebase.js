import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const app = firebase.initializeApp({
  apiKey: 'AIzaSyCGTEhMS6Hm3VdypH7m4temg_XjqQkSmBs',
  authDomain: 'subscription-api-2020a.firebaseapp.com',
  databaseURL: 'https://subscription-api-2020a.firebaseio.com',
  projectId: 'subscription-api-2020a',
  storageBucket: 'subscription-api-2020a.appspot.com',
  messagingSenderId: '499978986523',
  appId: '1:499978986523:web:514ad3a06ea0f3bc0804be'
});

export const auth = app.auth();
export default app;


