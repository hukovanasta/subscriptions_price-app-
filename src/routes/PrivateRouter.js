import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Home from "../pages/home/Home";

const PrivateRouter = () => {
  return (
    <>
      <Router>
        <Switch>
          <Redirect from="/auth" to="/" />
          <Route exact path="/" component={Home} />
        </Switch>
      </Router>
    </>
  )
}

export default PrivateRouter