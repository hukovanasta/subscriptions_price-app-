import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Login from "../pages/login/LogIn";
import SignUp from "../pages/signup/SignUp";

const PublicRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/auth/login" component={Login} />
        <Route exact path="/auth/signup" component={SignUp} />
        <Redirect from="/" to="/auth/login" />
      </Switch>
    </Router>
  )
}

export default PublicRouter