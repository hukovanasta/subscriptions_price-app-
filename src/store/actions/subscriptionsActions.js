import {
  FETCH_SUBSCRIPTIONS_IN_PROGRESS,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_SUBSCRIPTIONS_ERROR,
  SET_SUBSCRIPTION_IN_PROGRESS,
  SET_SUBSCRIPTION_SUCCESS,
  SET_SUBSCRIPTION_ERROR,
  POST_SUBSCRIPTION_IN_PROGRESS,
  POST_SUBSCRIPTION_SUCCESS,
  POST_SUBSCRIPTION_ERROR ,
  DELETE_SUBSCRIPTION_IN_PROGRESS,
  DELETE_SUBSCRIPTION_SUCCESS ,
  DELETE_SUBSCRIPTION_ERROR,
  PATCH_SUBSCRIPTION_SUBSCRIPTION_IN_PROGRESS,
  PATCH_SUBSCRIPTION_SUBSCRIPTION_SUCCESS,
  PATCH_SUBSCRIPTION_SUBSCRIPTION_ERROR
} from '../types';

import api from '../../plugins/axios/index';

export  function loadSubscriptions () {
  return async function(dispatch) {
    try {
      dispatch({ type: FETCH_SUBSCRIPTIONS_IN_PROGRESS })
      const subscriptionsData = await api.get(`/subscriptions`);
      dispatch({
        type: FETCH_SUBSCRIPTIONS_SUCCESS,
            payload: subscriptionsData.data
      })
    }
    catch (err) {
      dispatch({ type: FETCH_SUBSCRIPTIONS_ERROR })
    }
  }
}

export function loadSubscription(subscriptionId)  {
  return async function(dispatch) {
    try {
      dispatch({ type: SET_SUBSCRIPTION_IN_PROGRESS })
      const subscriptionData = await api.get(`/subscriptions/${subscriptionId}`);
      dispatch({
        type: SET_SUBSCRIPTION_SUCCESS,
        payload: {
          subscription: subscriptionData.data,
        }
      })
    }
    catch (err) {
      dispatch({ type: SET_SUBSCRIPTION_ERROR })
    }
  }
}

export  function postSubscriptions ( newSubscriptions ) {
  return async function(dispatch) {
    try {
      dispatch({ type: POST_SUBSCRIPTION_IN_PROGRESS })
      const subscriptionsData = await api.post(`/subscriptions`, newSubscriptions);
      dispatch({
        type: POST_SUBSCRIPTION_SUCCESS,
          payload: {
            subscriptions: subscriptionsData.data,
          }
      })
    }
    catch (err) {
      dispatch({ type: POST_SUBSCRIPTION_ERROR })
    }
  }
}

export function deleteSubscriptions (subscriptionId) {
  return async function(dispatch) {
    try {
      dispatch({ type: DELETE_SUBSCRIPTION_IN_PROGRESS })
      const subscriptionData = await api.delete(`/subscriptions/${subscriptionId}`);
      dispatch({
        type: DELETE_SUBSCRIPTION_SUCCESS,
          payload: {
              subscription: subscriptionData.data,
          }
        })
    }
    catch (err) {
      dispatch({ type: DELETE_SUBSCRIPTION_ERROR })
    }
  }
}

export function patchSubscriptions (subscriptionId) {
  return async function(dispatch) {
    try {
      dispatch({ type: PATCH_SUBSCRIPTION_SUBSCRIPTION_IN_PROGRESS })
      const subscriptionData = await api.patch(`/subscriptions/${subscriptionId}`);
      dispatch({
        type:  PATCH_SUBSCRIPTION_SUBSCRIPTION_SUCCESS,
          payload: {
            subscription: subscriptionData.data,
          }
        })
    }
    catch (err) {
      dispatch({ type: PATCH_SUBSCRIPTION_SUBSCRIPTION_ERROR })
    }
  }
}


