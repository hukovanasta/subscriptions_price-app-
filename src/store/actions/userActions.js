import {
    LOGIN_REQUEST_IN_PROGRESS,
    LOGIN_IN_REQUEST_SUCCESS,
    LOGIN_IN_REQUEST_ERROR,
    CHECK_AUTH_STATE_REQUEST_IN_PROGRESS,
    LOGOUT_REQUEST_IN_PROGRESS,
    LOGOUT_REQUEST_SUCCESS, LOGOUT_REQUEST_ERROR,
    SIGNUP_REQUEST_IN_PROGRESS, SIGNUP_IN_REQUEST_SUCCESS, SIGNUP_IN_REQUEST_ERROR
} from '../types';
import { getAuth, createUserWithEmailAndPassword,  signInWithEmailAndPassword, onAuthStateChanged, signOut } from "firebase/auth";

export function signUp(userData) {
  return async function(dispatch) {
    try {
      dispatch({ type: SIGNUP_REQUEST_IN_PROGRESS })
        const auth = getAuth();
        const res = await createUserWithEmailAndPassword(auth, userData.email, userData.password)
        dispatch({
          type:SIGNUP_IN_REQUEST_SUCCESS,
          payload: {
             email: res.user.email,
             uid: res.user.uid,
             accessToken: res.user.accessToken
          }
        })
    }
    catch (err) {
      dispatch({ type: SIGNUP_IN_REQUEST_ERROR })
    }
  }
}

export function logIn(userData) {
  return async function(dispatch) {
    try {
        dispatch({ type: LOGIN_REQUEST_IN_PROGRESS })
        const auth = getAuth();
        const res = await signInWithEmailAndPassword(auth, userData.email, userData.password)

        dispatch({
            type: LOGIN_IN_REQUEST_SUCCESS,
            payload: {
                email: res.user.email,
                uid: res.user.uid,
                accessToken: res.user.accessToken
            }
        })
    }
    catch (err) {
        dispatch({ type: LOGIN_IN_REQUEST_ERROR })
    }
  }
}

export function getAuthState() {
  return function (dispatch) {
    dispatch({ type: CHECK_AUTH_STATE_REQUEST_IN_PROGRESS })
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
      if (user) {
          dispatch({
              type: LOGIN_IN_REQUEST_SUCCESS,
              payload: {
                  email: user.email,
                  uid: user.uid,
                  accessToken: user.accessToken
              }
          })
          const token = user.accessToken;
          localStorage.setItem('token', token);
      } else {
          localStorage.removeItem('token');
          dispatch({ type: LOGIN_IN_REQUEST_ERROR })
      }
    })
  }
}

export function logOut() {
    return async function (dispatch) {
        try {
            dispatch({ type: LOGOUT_REQUEST_IN_PROGRESS })
            const auth = getAuth();
            await signOut(auth)
            localStorage.removeItem('token');
            dispatch({ type: LOGOUT_REQUEST_SUCCESS })
        } catch (err) {
            console.log(err)
            dispatch({ type: LOGOUT_REQUEST_ERROR })
        }
    }
}