import {
  FETCH_SUBSCRIPTIONS_IN_PROGRESS,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_SUBSCRIPTIONS_ERROR,
  SET_SUBSCRIPTION_IN_PROGRESS,
  SET_SUBSCRIPTION_SUCCESS,
  SET_SUBSCRIPTION_ERROR,
  POST_SUBSCRIPTION_IN_PROGRESS,
  POST_SUBSCRIPTION_SUCCESS,
  POST_SUBSCRIPTION_ERROR ,
  DELETE_SUBSCRIPTION_IN_PROGRESS,
  DELETE_SUBSCRIPTION_SUCCESS ,
  DELETE_SUBSCRIPTION_ERROR,
  PATCH_SUBSCRIPTION_SUBSCRIPTION_IN_PROGRESS,
  PATCH_SUBSCRIPTION_SUBSCRIPTION_SUCCESS,
  PATCH_SUBSCRIPTION_SUBSCRIPTION_ERROR
} from '../types';


const initState = {
  subscriptions: [],
  subscription: {},
  isloadSubscriptionsInProgress: false,
  isloadSubscriptionInProgress: false,
  ispostSubscriptionsInProgress: false,
  isdeleteSubscriptionsInProgress: false,
  ispatchSubscriptionsInProgress: false
};

const subscriptionsReducer = (state = initState, action) => {
  switch (action.type) {
    case  FETCH_SUBSCRIPTIONS_IN_PROGRESS:
      return {
        ...state,
        isloadSubscriptionsInProgress: true
      };
    case FETCH_SUBSCRIPTIONS_SUCCESS:
      return {
        ...state,
        subscriptions: action.payload,
      };
    case  FETCH_SUBSCRIPTIONS_ERROR:
      return {
        ...state,
        isloadSubscriptionsInProgress: false
      };
    case SET_SUBSCRIPTION_IN_PROGRESS:
      return {
        ...state,
        isloadSubscriptionInProgress: true
      }
    case SET_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        subscription: action.payload,
      };
    case  SET_SUBSCRIPTION_ERROR:
      return {
        ...state,
        isloadSubscriptionsInProgress: false
      };
    case POST_SUBSCRIPTION_IN_PROGRESS:
      return {
        ...state,
        ispostSubscriptionsInProgress: true
      }
    case POST_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        subscriptions: [action.payload, ...state.subscriptions]
      };
    case POST_SUBSCRIPTION_ERROR:
      return {
        ...state,
        ispostSubscriptionsInProgress: false
      };
    case DELETE_SUBSCRIPTION_IN_PROGRESS:
      return {
        ...state,
        isdeleteSubscriptionsInProgress: true
      }
    case DELETE_SUBSCRIPTION_SUCCESS:
      let index = state.subscriptions.findIndex(
        (subscription) => subscription.subscriptionId === action.payload
      );
      state.subscriptions.splice(index, 1);
      return {
        ...state,
        subscription: action.payload
      };
    case DELETE_SUBSCRIPTION_ERROR:
      return {
        ...state,
        isdeleteSubscriptionsInProgress: false
      };
    case PATCH_SUBSCRIPTION_SUBSCRIPTION_IN_PROGRESS:
      return {
        ...state,
        ispatchSubscriptionsInProgress: true
      }
    case PATCH_SUBSCRIPTION_SUBSCRIPTION_SUCCESS:
      let indexSubscription = state.subscriptions.findIndex(
        (subscription) => subscription.subscriptionId === action.payload.subscriptionId
      );
      state.subscriptions[indexSubscription] = action.payload;
      if (state.subscription.subscriptionId === action.payload.subscriptionId) {
        state.subscription = action.payload;
      }
      return {
        ...state,
      };
    case PATCH_SUBSCRIPTION_SUBSCRIPTION_ERROR:
      return {
        ...state,
        ispatchSubscriptionsInProgress: false
      };
    default:
      return { ...state };
    }
  };


export default  subscriptionsReducer;