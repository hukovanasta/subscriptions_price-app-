import {
  LOGIN_REQUEST_IN_PROGRESS,
  LOGIN_IN_REQUEST_SUCCESS,
  LOGIN_IN_REQUEST_ERROR,
  CHECK_AUTH_STATE_REQUEST_IN_PROGRESS,
  LOGOUT_REQUEST_IN_PROGRESS, LOGOUT_REQUEST_SUCCESS, LOGOUT_REQUEST_ERROR,
  SIGNUP_REQUEST_IN_PROGRESS, SIGNUP_IN_REQUEST_SUCCESS, SIGNUP_IN_REQUEST_ERROR
} from '../types';

const initialUserState = {
  authenticated: false,
  loading: false,
  currentUser: null,
  isLoginInProgress: false,
  isSignupInProgress: false,
  isCheckAuthStateInProgress: false,
  isLogOutInProgress: false
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function(state = initialUserState, action) {
  switch (action.type) {
    case LOGIN_REQUEST_IN_PROGRESS:
      return {
        ...state,
        isLoginInProgress: true
      };
    case  SIGNUP_REQUEST_IN_PROGRESS:
      return {
        ...state,
        isSignupInProgress: true
      }
    case CHECK_AUTH_STATE_REQUEST_IN_PROGRESS:
      return {
        ...state,
        isCheckAuthStateInProgress: true
      };
    case LOGIN_IN_REQUEST_SUCCESS:
      return {
        ...state,
        isLoginInProgress: false,
        isCheckAuthStateInProgress: false,
        authenticated: true,
        currentUser: { ...action.payload }
      }
    case  SIGNUP_IN_REQUEST_SUCCESS:
      return {
        ...state,
        isSignupInProgress: false,
        isCheckAuthStateInProgress: false,
        authenticated: true,
        currentUser: { ...action.payload }
      }
    case LOGIN_IN_REQUEST_ERROR:
      return {
        ...state,
        isLoginInProgress: false,
        isCheckAuthStateInProgress: false,
        authenticated: false,
        currentUser: null
      };
    case SIGNUP_IN_REQUEST_ERROR:
      return {
        ...state,
        isSignupInProgress: false,
        isCheckAuthStateInProgress: false,
        authenticated: false,
        currentUser: null

      }
    case LOGOUT_REQUEST_IN_PROGRESS:
      return {
        ...state,
        isLogOutInProgress: true
      };
    case LOGOUT_REQUEST_SUCCESS:
      return {
        ...state,
        currentUser: null,
        isLogOutInProgress: false
      };
    case LOGOUT_REQUEST_ERROR:
      return {
        ...state,
        isLogOutInProgress: false
      };
    default:
      return {
        ...state
      }
  }
}